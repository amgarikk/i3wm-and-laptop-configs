My i3wm and polybar configs plus dots.

- i3 keybindings include and not limited to:

* [dunst](https://github.com/dunst-project/dunst)

* [rofi](https://github.com/davatorium/rofi)

* [polybar](https://github.com/polybar/polybar)
