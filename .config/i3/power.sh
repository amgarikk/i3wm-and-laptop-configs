selected=$(echo -e $entries|rofi -dmenu -theme Indego | awk '{print tolower($2)}')

case $selected in

  suspend)
    exec systemctl suspend;;
  logout)
    exec killall i3;;
  reboot)
    exec systemctl reboot;;
  shutdown)
    exec systemctl poweroff -i;;
esac