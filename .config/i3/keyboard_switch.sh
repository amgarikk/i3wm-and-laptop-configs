#!/bin/sh

layout_f=/tmp/kb_layout

case $(cat "$layout_f") in 
  us)
    setxkbmap -layout kz && setxkbmap -layout kz,us
    echo kz > $layout_f
    ;;
  kz)
    setxkbmap -layout us && setxkbmap -layout us,kz
    echo us > $layout_f
    ;;
esac
